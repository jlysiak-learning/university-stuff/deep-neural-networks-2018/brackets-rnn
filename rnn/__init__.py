from .utils import  generate_to_file_random
from .utils import read_examples_from_file

from .rnn import RNN

from .conf import CLI_ARGS 

