import json

# Config/CLI args with defaults values and kw dicts
ARGS = [
    # === NETWORK STATE + STUFF
    (
        "ckpt_name", 'rnn', 
        None),
    (
        "conf_name", 'config.json', 
        None),
    (
        "plot_data_file", "plot_data.log",
        None),
    # === NETWORK CONFIG
    (
        "out_sz", 3, 
        None),
    (
        "valid_dataset", None,
        {'metavar': 'PATH', 'help': 'Validation dataset file'}),
    (
        "train_dataset", None,
        {'metavar': 'PATH', 'help': 'Training dataset file'}),
    (
        "epochs", 200,
        {'metavar': 'PATH', 'help': 'Number of training epochs', 'type':int}),
    (
        "batches_per_epoch", 5000,
        {'metavar': 'PATH', 'help': 'Number of batches in one epoch', 'type':int}),
    (
        "batch_size", 32,
        {'metavar': 'PATH', 'help': 'Training batch size', 'type':int}),
    (
        "use_lstm", 'n',
        {'metavar': 'y/n', 'help': 'Use LSTM cell'}),
    (
        'valid_examples_n', 5000,
        {'metavar': 'N', 'help': 'Size of validation dataset', 'type':int}),
    (
        "learning_rate", 0.0001,
        {'metavar': 'R', 'help': 'Learning rate', 'type': float}),
    (
        "cell_size", 128,
        {'metavar': 'PATH', 'help': 'Cell size', 'type':int}),
    (
        'sequence_length_min', 24,
        {'metavar': 'N', 'help': 'Minimal length of input sequence', 'type': int}),
    (
        'sequence_length_max', 64,
        {'metavar': 'N', 'help': 'Maximal (inclusive) length of input sequence', 'type': int}),
    (   
        'predict_on', None,
        {'metavar': 'FILE', 'help': 'Generate predictions for given files', 'nargs': '*'}),
    (   
        'show_predictions', False,
        {'action':'store_true', 'help': 'Show predicted values and labels on outpt'}),
    (
        "predict", None,
        {'metavar': 'NETWORK_DIR', 'help': 'Run prediction on given network', 
            'nargs': '*'}),
    (
        "train", None,
        {'metavar': 'NETWORK_DIR', 'help': 'Train network', 'nargs': '*'})
]

DEFAULT_CONF = dict([(i, j) for i, j, _ in ARGS])
CLI_ARGS = dict([(i, j) for i, _, j in ARGS if j is not None])


def get_conf(path, f):
    if not os.path.exists(path):
        print("Creating new configuration file: " + path)
        conf = create_conf(f)
        save_config(path, conf)
    else:
        try:
            conf = load_config(path)
        except Exception:
            print("Cannot open configuration file: " + path)
            exit(1)
    return conf

    

def save_config(path, config):
    """
    Create default network configuration.
    """
    s = json.dumps(config, sort_keys=True, indent=4)
    with open(path, 'w') as f:
        f.write(s)


def create_default_config(path):
    """
    Create default network configuration.
    """
    save_config(path, DEFAULT_CONFIG)


def load_config(path):
    """
    Load network configuration from JSON file
    """
    with open(path, 'r') as f:
        r = f.read()
    return json.loads(r)

