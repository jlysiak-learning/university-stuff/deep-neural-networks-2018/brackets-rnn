"""
Recurrent Neural Network wrapper
Jacek Łysiak - DNN @ MIMUW 2017/18
"""

import tensorflow as tf
from tensorflow.python.ops import rnn, rnn_cell
import os
import time
import numpy as np
import sys

from .conf import DEFAULT_CONF, save_config, load_config
from .utils import read_examples_from_file, generate_examples_random, to_one_hot
from .utils import calc_stats

class FLAGS():
    def __init__(self, d):
        self.__dict__ = d


class RNN():

    def __init__(self, opts=None):
        conf = opts 
        train = conf['train']
        predict = conf['predict']
        path = None if train is None else train
        path = path if predict is None else predict 
        saving = True
        conf = DEFAULT_CONF
        if path is None:
            self.log("Using temporary mode. State will NOT be saved!")
            saving = False
        else:
            self.log("Using saved network: " + path)
            if os.path.exists(path):
                conf = load_config(os.path.join(path, DEFAULT_CONF['conf_name'])) 
            else:
                os.makedirs(path)
         
        for k, v in opts.items():
            if v is not None:
                conf[k] = v

        if path is not None:
            if not os.path.exists(os.path.join(path, DEFAULT_CONF['conf_name'])):
                conf['ckpt_pref'] = os.path.join(path, conf['ckpt_name'])
                conf['plot_data_file'] = os.path.join(path, conf['plot_data_file'])
                fn = os.path.join(path, DEFAULT_CONF['conf_name'])
                save_config(fn, conf)

        self.conf = conf
        self.saving = saving
        self.print_configuration()    

        self._build_network()

    def log(self, s):
        sys.stderr.write(s + "\n")


    def _build_network(self):
        OPTS = FLAGS(self.conf) 

        W = tf.Variable(tf.random_normal([OPTS.cell_size, OPTS.out_sz]))    
        b = tf.Variable(tf.random_normal([OPTS.out_sz]))    

        # Sequences - one-hot encoded
        # None - batch size, None - sequence length
        x = tf.placeholder(tf.float32, [None, None, 2])
        # Sequnces lengths
        lengths = tf.placeholder(tf.int32, [None])
        # Correct sequence statistics
        y = tf.placeholder(tf.float32, [None, OPTS.out_sz])

        self.x = x
        self.y = y
        self.lengths = lengths

        
        if OPTS.use_lstm:
            cell = rnn_cell.BasicLSTMCell(OPTS.cell_size, state_is_tuple=True)
        else:
            cell = rnn_cell.BasicRNNCell(OPTS.cell_size)
        outputs, states = rnn.dynamic_rnn(cell=cell,
                inputs=x, 
                sequence_length=lengths,
                dtype=tf.float32)
        b_range = tf.range(tf.shape(x)[0])
        inds = tf.stack([b_range, lengths - 1], axis=1)
        last_outputs = tf.gather_nd(outputs, inds) 
        output = tf.matmul(last_outputs, W) + b
        eq = tf.equal(tf.to_int32(output), tf.to_int32(y))
        self.acc = tf.reduce_mean(tf.to_float(eq))
        y = y + tf.constant(0.1)
        self.loss = tf.losses.mean_squared_error(labels=y, predictions=output)
        self.step = tf.train.AdamOptimizer(OPTS.learning_rate).minimize(self.loss)
        self.prediction = tf.to_int32(output)


    def get_dataset(self, phase):
        """
        Returns validation or training datasets
        """
        OPTS = FLAGS(self.conf) 
        is_valid = phase == 'valid'
        dataset = OPTS.valid_dataset if is_valid else OPTS.train_dataset

        batches = 1 if is_valid else OPTS.batches_per_epoch
        batch_sz = OPTS.valid_examples_n if is_valid else OPTS.batch_size
        n = batches * batch_sz
        l_min = OPTS.sequence_length_min
        l_max = OPTS.sequence_length_max

        if dataset is not None:
            self.log("Reading data `%s` set from file: %s" %(phase, dataset))
            with open(dataset, 'r') as f:
                seqs, stats, ls = read_examples_from_file(f, n, with_stat=True)
        else:
            self.log("Generating `%s` dataset..." % phase)
            seqs, stats, ls = generate_examples_random(l_min, l_max, n, with_stat=True)

        seqs = np.reshape(seqs, (batches, batch_sz, -1))
        stats = np.reshape(stats, (batches, batch_sz, -1))
        ls = np.reshape(ls, (batches, batch_sz))
        return seqs, stats, ls
        

    def print_configuration(self):
        self.log("=" * 10 + "CONFIGURATION")
        for k, v in self.conf.items():
            self.log("{}: {}".format(k, str(v)))
        self.log("=" * 20 )


    def train(self):
        self.log("Training...")

        OPTS = FLAGS(self.conf) 

        saver = tf.train.Saver()

        vx, vy, vls = self.get_dataset('valid')
        vx = to_one_hot(vx[0])
        vy = vy[0]
        vls = vls[0]
        batches_x, batches_y, batches_ls = self.get_dataset('train')
        batches_x = to_one_hot(batches_x)

        restored = False
        with tf.Session() as sess:
            if self.saving:
                if tf.train.checkpoint_exists(OPTS.ckpt_pref):
                    self.log("Checkpoint detected in %s! Restoring ..." % OPTS.ckpt_pref)
                    saver.restore(sess, OPTS.ckpt_pref)
                    restored = True
                else:
                    self.log("No checkpoint exists in %s" % OPTS.ckpt_pref)

            if not restored:
                self.log("Initializing new variables")
                tf.global_variables_initializer().run()
            stamp = time.strftime('%Y-%m-%d-%H-%M-%S', time.localtime())
            self.log("Started at " + stamp)

            time_start = time.time()
            if self.saving:
                plot_data_file = open(OPTS.plot_data_file, 'w')
            try:
                for epoch in range(OPTS.epochs):
                    for dx, dy, l in zip(batches_x, batches_y, batches_ls):    
                        feed = {self.x: dx, self.y: dy, self.lengths: l}
                        sess.run([self.step], feed_dict=feed)
                    self.log("Epochs done: %d / %d" % (epoch + 1, OPTS.epochs))
                    feed = {self.x: vx, self.y: vy, self.lengths: vls}
                    acc, loss = sess.run([self.acc, self.loss], feed_dict=feed)
                    time_elapsed = time.time() - time_start

                    stamp = time.strftime('%Y-%m-%d-%H-%M-%S', time.localtime())
                    self.log("[%s] Validation results: loss=%f acc=%f" % (stamp, loss, acc))
                    if self.saving:
                        plot_data_file.write("{} {} {} {} {}\n".format(stamp, epoch + 1,
                            loss, acc, time_elapsed))
            except KeyboardInterrupt:
                self.log("Training interrupted by user!")
            if self.saving:
                self.log("Model saved: %s" % saver.save(sess, OPTS.ckpt_pref))
                plot_data_file.close()


    def predict(self):
        OPTS = FLAGS(self.conf) 

        saver = tf.train.Saver()
        if not tf.train.checkpoint_exists(OPTS.ckpt_pref):
            self.log("No checkpoint in %s" % OPTS.ckpt_pref)
            exit(1)

        files = []
        if OPTS.predict_on is None:
            interactive = True
            files = [None]
        else:
            files = OPTS.predict_on
            interactive = OPTS.show_predictions

        with tf.Session() as sess:
            saver.restore(sess, OPTS.ckpt_pref)
            try:
                for input_name in files:
                    if input_name is not None:
                        f = open(input_name, 'r')
                    else:
                        f = sys.stdin
                    vec_correct = 0
                    vals_correct = 0
                    total = 0
                    se = 0
                    while True:
                        data = read_examples_from_file(f, 1)
                        if data is None:
                            break
                        ls = data[2]
                        dx = to_one_hot(data[0])
                        dy = calc_stats(data[0], ls)
                        feed = {self.x: dx, self.y: dy, self.lengths: ls}
                        pred = sess.run(self.prediction, feed_dict=feed)
                        correct = np.sum(np.equal(pred, dy))
                        vals_correct += correct
                        total += 1
                        if correct == 3:
                            vec_correct += 1
                        se += np.sum(np.square(pred - dy))

                        if interactive:
                            s  = ""
                            for el in pred[0]:
                                s += "{} ".format(el)
                            for el in dy[0]:
                                s += "{} ".format(el)
                            print(s)

                    if total > 0:
                        self.log("NAME TOTAL MSE ACC_VEC ACC_VAL")
                        print("{name} {total} {mse} {acc_vec} {acc_val}".format(
                            name=input_name,
                            total=total,
                            mse=se / total,
                            acc_vec=vec_correct / total,
                            acc_val=vals_correct / total / 3.0))
                    if f != sys.stdin:
                        f.close()
            except KeyboardInterrupt:
                pass
                
