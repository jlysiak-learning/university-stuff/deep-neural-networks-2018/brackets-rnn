"""
Bunch of utilities
"""

import numpy as np


def calc_stats(seqs, ls):
    """
    Calc simple statistics.
    Args:
        seqs: array of {-1, 1} sequence
        ls: seqences lengths
    Returns:
        array of max ( ) distance, max consequtive opennings, max opened brackets
    """
    stats = np.zeros((seqs.shape[0], 3))
    i = 0
    for arr, l in zip(seqs, ls):
        m_con = 0
        m_op = 0
        m_dist = 0

        last = 0

        op = 0
        con = 0
        dist = 0
        for idx, e in enumerate(arr):
            if idx == l:
                break
            op += e
            m_op = max(m_op, op)

            if op > 0:
                dist += 1
            else:
                m_dist = max(m_dist, dist)
                dist = 0

            if e == last:
                con += 1
            else:
                m_con = max(m_con, con)
                con = 0
            last = e
        m_op = int(m_op)
        stats[i] = np.array([m_dist - 1, m_con + 1, m_op])
        i += 1
    return stats.astype(np.int32)


def output(seqs_arr, stats_arr, ls, f):
    add_stat = True if stats_arr is not None else False
    i = 0
    for seq, l in zip(seqs_arr, ls):
        seq = "".join([i for i in map(lambda x: '(' if x == 1 else ')', seq[:l])])
        if add_stat:
            d = stats_arr[i][0]
            c = stats_arr[i][1]
            o = stats_arr[i][2]
            s = "{} {} {} {}\n".format(seq, d, c, o)
        else:
            s = seq + '\n'
        f.write(s)
        i += 1


def to_one_hot(arr):
    """
    Convert array of -1,1 sequences into one-hot representation.
    Args:
        arr: numpy array sequences
    Returns:
        one-hot encoded sequences
    """
    return np.eye(2)[np.where(arr > 0, 0, 1)]


def read_examples_from_file(f, lines_n, with_stat=False):
    """
    Read given number of examples from file.
    Args:
        f: opened file
        lines_n: lines to read
        with_stat: read also statistics
    Returns:
        tuple of arrays: sequences of 1, -1; stats; lengths

    """
    seqs = []
    stats = []
    ls = []
    l_max = 0
    for i in range(lines_n):
        line = f.readline()
        if line == '':
            return None
        line = line.split(' ')
        if with_stat:
            stats += [[i for i in map(lambda x: int(x), line[1:])]]
        line = line[0].strip()
        l = len(line)
        seqs += [[i for i in map(lambda x: 1 if x == '(' else -1, line)]]
        ls += [l]
        l_max = max(l, l_max)
    _seqs = np.zeros([lines_n, l_max])
    ls = np.array(ls, dtype=np.int32)
    for i, j in enumerate(seqs):
        _seqs[i][0:ls[i]] = j
    if not with_stat:
        stats = None
    else:
        stats = np.array(stats)
    return _seqs, stats, ls


def generate_examples_random(min_seq_len, max_seq_len, seq_n, with_stat=False, op_p=0.5):
    """
    Generate `seq_n` sequences of given length `seq_len` from {-1, 1}
    in random fashion.
    Args:
        min_seq_len: min length inclusive
        max_seq_len: max length invlusive
        seq_n: number of sequences to generate
        with_stat: append statistics (labels) to data
    Returns:
        (array with -1,1 sequences, array of statistics)
        Stat arr is None if with_stat is False
    """
    arr = np.zeros(max_seq_len)
    seqs = []
    ls = []
    choice = [i for i in range(min_seq_len, max_seq_len + 1, 2)]
    for _ in range(seq_n):
        op = 0
        seq_len = np.random.choice(choice)
        ls += [seq_len]
        for i in range(seq_len):
            if seq_len - i == op:
                arr[i] = -1
                op -= 1
            else:
                if op > 0:
                    p = np.random.choice([0, 1], p=[1. - op_p,op_p])
                    el = 1 if p > 0.5 else -1
                    arr[i] = el
                    op += el
                else: # op == 0
                    arr[i] = 1
                    op += 1
            assert op >= 0, "Too much closed brackets"
        assert op == 0, "Opend brackets is non zero value"
        seqs += [np.copy(arr)]
        arr[:] = 0
    seqs = np.array(seqs)
    ls = np.array(ls)
    stats = None
    if with_stat:
        stats = calc_stats(seqs, ls)
    return seqs, stats, ls


def generate_to_file_random(min_seq_len, max_seq_len, seq_n, f, with_stat=False, op_p=0.5):
    """
    Generate `seq_n` sequences of given length in range `min_seq_len` - `max_seq_len` 
    into opened file `f` in random fashion.
    Args:
        min_seq_len: length
        max_seq_len: max length
        seq_n: number of sequences to generate
        f:  opened file
        with_stat: append statistics (labels) to data
    """
    M = 1000000
    while seq_n > 0:
        k = seq_n if seq_n < M else M
        seqs, stats, ls = generate_examples_random(min_seq_len, max_seq_len, k, with_stat, op_p)
        output(seqs, stats, ls, f)
        seq_n -= k


