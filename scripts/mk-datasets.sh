#!/bin/bash
#
# Generate files with sequences of brackets


DATASETS_DIR=datasets

TRAIN_EX=160000
VALID_EX=5000
PRED_EX=40

rm -rf ${DATASETS_DIR}
mkdir ${DATASETS_DIR}

echo Generating sequences of length: 24
./generator -a -s -L 1000000 -o ${DATASETS_DIR}/t24.tmp -l 24
cat ${DATASETS_DIR}/t24.tmp | tail -n ${VALID_EX} > ${DATASETS_DIR}/v24
cat ${DATASETS_DIR}/t24.tmp | head -n ${TRAIN_EX} > ${DATASETS_DIR}/t24
rm ${DATASETS_DIR}/t24.tmp
./generator -s -L ${PRED_EX} -o ${DATASETS_DIR}/p24 -l 24

for l in 32 40 48 56 64;
do
        echo Generating sequences of length: ${l}
       ./generator -s -L ${VALID_EX} -o ${DATASETS_DIR}/v${l} -l ${l}
       ./generator -s -L ${TRAIN_EX} -o ${DATASETS_DIR}/t${l} -l ${l}
       ./generator -s -L ${PRED_EX} -o ${DATASETS_DIR}/p${l} -l ${l}

done
