#!/bin/bash
# 
# Create new RNN networks with different parameters
# and train them once.

GPU_RUN="srun  -u -t 12:00:00  --gres=gpu:1 --mem 11000 /home/jl345639/mgr-tf-gpu/bin/python3.5 trainer"
LOCAL_RUN="./trainer"

if [ "$1" == "gpu" ];
then
        RUN="${GPU_RUN}"
else
        RUN=${LOCAL_RUN}
fi

MODELS_DIR=models
DATASETS_DIR=datasets

EPOCHS=10
BATCH_SZ=32
BATCHES_PER_EPOCH=100
VALID_EX=1000
LEARNING_RATE=0.001

mkdir -p ${MODELS_DIR}

for l in 24 32 40 48 56 64;
do
        for t in y n;
        do
                NAME="${t}_${l}"
                FLAGS="--valid_dataset ${DATASETS_DIR}/v${l}    \
                        --train_dataset ${DATASETS_DIR}/t${l}   \
                        --epochs ${EPOCHS}                      \
                        --batches_per_epoch ${BATCHES_PER_EPOCH}\
                        --batch_size ${BATCH_SZ}                \
                        --sequence_length ${l}                  \
                        --use_lstm ${t}                         \
                        --valid_examples_n ${VALID_EX}          \
                        --learning_rate ${LEARNING_RATE}        \
                        --train ${MODELS_DIR}/${NAME}"
                ${RUN} ${FLAGS}
                if [ $? != 0 ];
                then
                        exit
                fi
        done
done
