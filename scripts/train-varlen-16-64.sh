#!/bin/bash
# 
# Create new RNN networks with different parameters
# and train them once.

GPU_RUN="srun  -u -t 12:00:00  --gres=gpu:1 --mem 11000 /home/jl345639/mgr-tf-gpu/bin/python3.5 trainer"
LOCAL_RUN="./trainer"

if [ "$1" == "gpu" ];
then
        RUN="${GPU_RUN}"
else
        RUN=${LOCAL_RUN}
fi

MODELS_DIR=models
DATASETS_DIR=datasets

EPOCHS=200
BATCH_SZ=32
BATCHES_PER_EPOCH=5000
VALID_EX=5000
LEARNING_RATE=0.001

mkdir -p ${MODELS_DIR}

NAME="${2}_varlen"
FLAGS="--valid_dataset ${DATASETS_DIR}/v_16-64_50000    \
        --train_dataset ${DATASETS_DIR}/t_16-64_5000000   \
        --epochs ${EPOCHS}                      \
        --batches_per_epoch ${BATCHES_PER_EPOCH}\
        --batch_size ${BATCH_SZ}                \
        --use_lstm ${2}                         \
        --valid_examples_n ${VALID_EX}          \
        --learning_rate ${LEARNING_RATE}        \
        --train ${MODELS_DIR}/${NAME}"
${RUN} ${FLAGS}
if [ $? != 0 ];
then
        exit
fi
