#!/bin/bash

for i in `ls models`;
do
        echo "Running $i"
        ./trainer --predict models/$i --predict_on test-datasets/*/* > test-results/${i}.log
done
